import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:no_name/dashboard.dart';

class settings extends StatefulWidget {
  @override
  _settingsState createState() => _settingsState();
}

class _settingsState extends State<settings> {
  var title = 'Settings';
  String valuechoose;
  List<String> fonts = [
    'Benne','NotoSansJP','PTSans','Roboto','Vollkorn'
  ];
  List<String> type = ['Bold','Italic'];
  List<bool> selection = [false,false];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appbar(title,Icons.arrow_back, Icons.info_outline, context),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(20.0, 15.0, 0.0, 20.0),
            child: DropdownButton(
                value: valuechoose,
              hint: Text('Select Font'),
              onChanged: (newvalue){
                  setState(() {
                    valuechoose = newvalue;
                  });
              },
              items: fonts.map((valueItem)
                {
                  return DropdownMenuItem(
                    value: valueItem,
                      child:Text(valueItem)
                  );
                }).toList(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(20.0, 15.0, 0.0, 20.0),
            child: showtext(valuechoose,selection[0],selection[1]),
          ),
          Padding(
            padding: const EdgeInsets.all(25.0),
            child: ToggleButtons(children: [
              Text('Bold'),
              Text('Italic'),
            ],
                isSelected: selection,
              onPressed: (int index) {
              setState(() {
                selection[index] = !selection[index];
              });


              }//onpressed
            ),
          ),
        ],
      ),
    );
  }
}

Widget showtext(var font,bool bold,bool italic){
  return Text(
    'Sample text',
    style: TextStyle(
      fontSize: 50.0,
      fontFamily: font,
      fontWeight: bold? FontWeight.bold : null,
      fontStyle: italic? FontStyle.italic : null,
    ),
  );
}