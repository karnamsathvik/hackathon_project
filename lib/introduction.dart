import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';

class Intro_pages extends StatelessWidget {
  final pageDecoration = PageDecoration(
    titleTextStyle:
        PageDecoration().titleTextStyle.copyWith(color: Colors.black),
    bodyTextStyle:
        PageDecoration().titleTextStyle.copyWith(color: Colors.black),
  );

  List<PageViewModel> getpages() {
    return [
      PageViewModel(
        image: Image.asset('assets/email.png'),
        titleWidget: Text('  '),

        body:
            'Open moniter and send scheduled mail.All over your organisation',
        decoration: pageDecoration,
      ),
      PageViewModel(
        image: Image.asset('assets/computer.png'),
        titleWidget: Text('  '),
        body:
            'Never sacrifice these features: security,flexibility and speed. Now, you\'ve got em all',
        decoration: pageDecoration,
      ),
      PageViewModel(
        titleWidget: Text('   '),
        body: 'Get creative and start designing mails using different font styles and colors',
        decoration: pageDecoration,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: IntroductionScreen(
          globalBackgroundColor: Colors.white,
          pages: getpages(),
          done: Text('Done'),
          onDone: () {
            Navigator.pop(context);
          },
          showNextButton: true,
          next: Text('Next'),
          skip: Text("Skip"),
          showSkipButton: true,
        ),
      ),
    );
  }
}
