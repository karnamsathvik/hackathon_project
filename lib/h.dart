import 'package:flutter/material.dart';
import 'package:no_name/dashboard.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:no_name/email_show.dart';

import 'Email.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  var title = 'Home Page';

  final _firestore = FirebaseFirestore.instance;
  final _auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appbar(title,Icons.account_circle_outlined, Icons.info_outline, context),
      body: StreamBuilder<QuerySnapshot>(
          stream: _firestore.collection('emails').snapshots(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final emails = snapshot.data.docs;
              List<Widget> messageWidgets = [];
              for (var message in emails) {
                if(message.get('to')==_auth.currentUser.email){
                  final email = Email(message.get('body'), message.get('to'),
                      message.get('from'), message.get('subject'));
                  final messageWidget = emailTile(context, email);
                  messageWidgets.add(messageWidget);
                }
              }
              return Column(children: messageWidgets);
            }
            return Container();
          }),

    );}

  Widget emailTile(BuildContext context, Email e) {
    return TextButton(
      onPressed: () {
        showModalBottomSheet(
            backgroundColor: Colors.transparent,
            context: context,
            builder: (BuildContext context) {
              return Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                    color: Colors.blue[100]),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10,),
                    Hero(
                      tag: 'avatar',
                      child: Center(
                          child:
                          CircleAvatar(child: Text(e.from[0].toUpperCase(),style: TextStyle(fontSize: 30),),
                            radius: 30,)),
                    ),
                    SizedBox(height: 10,),
                    Center(
                        child: Text(
                          e.from,
                          style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                        )),
                    SizedBox(height: 20,),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16,0,16,0),
                      child: Text("message:",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16,0,16,0),
                      child: Text(
                        e.body,
                        style: TextStyle(fontSize: 20),
                      ),
                    )
                  ],
                ),
              );
            });
      },
      child: Container(

        margin: EdgeInsets.fromLTRB(8, 8, 8, 0),
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3), // changes position of shadow
              ),
            ],
            borderRadius: BorderRadius.all(Radius.circular(20)),
            color: Colors.blue[100]),
        child: ListTile(
          leading: Hero(
              tag: 'avatar',
              child: CircleAvatar(child: Text(e.from[0].toUpperCase()))),
          title: Hero(
              tag: 'subject',
              child: Text(
                e.subject,
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              )),
          subtitle: Hero(tag: 'from', child: Text(e.from)),
        ),
      ),
    );
  }
}
