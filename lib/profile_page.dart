import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:no_name/signin.dart';
import 'dart:io';

class profile extends StatelessWidget {
  final _auth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              CircleAvatar(
                radius: 80.0,
                child: Text(_auth.currentUser.email[0],
                style: TextStyle(
                  fontSize: 100.0,
                ),),
              ),
              Text(_auth.currentUser.email,
              style: TextStyle(
                fontSize: 25.0,
              ),),
              RaisedButton(onPressed: (){
                _auth.signOut();
                exit(0);
              },
              child: Text('sign out and exit'),
                color: Colors.redAccent,
              )
            ],
          ),
        ),
      ),
    );
  }
}
