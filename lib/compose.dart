import 'package:flutter/material.dart';
import 'package:no_name/dashboard.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Compose extends StatefulWidget {
  @override
  _ComposeState createState() => _ComposeState();
}

class _ComposeState extends State<Compose> {
  var title = "Compose";
  String from, to, subject, body;

  final _firestore = FirebaseFirestore.instance;
  final _tocontroller = TextEditingController();
  final _subjectcontroller = TextEditingController();
  final _bodycontroller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: appbar("",Icons.account_circle_outlined ,Icons.settings, context),
      body: ClipRRect(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30), topRight: Radius.circular(30)),
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    SizedBox(
                      width: 50,
                    ),
                    Expanded(
                        child: Center(
                            child:
                                Text(title, style: TextStyle(fontSize: 20)))),
                    sendButton()
                  ],
                ),
                TextFormField(
                  textCapitalization: TextCapitalization.words,
                  decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: ' From',
                  ),
                  onChanged: (e) {
                    from = e;
                  },
                ),
                TextFormField(
                  textCapitalization: TextCapitalization.words,
                  decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: ' To',
                  ),
                  controller: _tocontroller,
                  onChanged: (e) {
                    to = e;
                  },
                ),
                TextFormField(
                  textCapitalization: TextCapitalization.words,
                  decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: ' Subject',
                  ),
                  controller: _subjectcontroller,
                  onChanged: (e) {
                    subject = e;
                  },
                ),
                Expanded(
                  child: TextField(
                    maxLines: 50,
                    decoration: InputDecoration(
                      hintText: ' Compose',
                    ),
                    controller: _bodycontroller,
                    onChanged: (e) {
                      body = e;
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget sendButton() {
    return IconButton(
        iconSize: 25,
        icon: Icon(
          Icons.send,
          color: Colors.blue,
        ),
    onPressed: (){
      _firestore.collection('emails').add({
        'from':from,
        'to':to,
        'subject':subject,
        'body':body
      });
      _tocontroller.clear();
      _subjectcontroller.clear();
      _bodycontroller.clear();
    },);
  }
}
