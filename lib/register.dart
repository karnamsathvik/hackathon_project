import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _auth = FirebaseAuth.instance;
  String email, password;
  bool showSpinner = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: showSpinner,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Text("Register NOW"),
                Expanded(
                  flex: 2,
                  child: Center(
                    child: TextField(
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (value) {
                        email = value; //Taking Email
                      },
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: TextField(
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    onChanged: (v) {
                      password = v; //taking password
                    },
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: IconButton(
                    icon: Icon(Icons.send),
                    iconSize: 60,
                    onPressed: () {
                      setState(() {
                        showSpinner=true;
                      });
                      onRegesteringIn(context);
                    },
                  ),
                ),
                TextButton(onPressed: () {
                  Navigator.pop(context);
                }, child: Text("Have an account, sign in"))
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onRegesteringIn(BuildContext context) async {
    try {
      final newUser = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      if (newUser != null) {
        Navigator.pop(context);
      }
    } catch (e) {
      print(e);
    }
    setState(() {
      showSpinner=false;
    });
  }
}


